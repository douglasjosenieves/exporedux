import React, { Component } from "react";
import { StyleSheet, Text, View, AsyncStorage } from "react-native";
import { createStore, combineReducers, applyMiddleware } from "redux";
import { Provider, connect } from "react-redux";
import { persistStore, persistReducer } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";
import axios from "axios";
import axiosMiddleware from "redux-axios-middleware";
import { multiClientMiddleware } from "redux-axios-middleware";
//import AsyncStorage from "@react-native-community/async-storage";
import { composeWithDevTools } from "redux-devtools-extension";

import reducer from "./reducers";
import Router from "./Router";
import { createLogger } from "redux-logger";

import * as Font from "expo-font";
import { Ionicons } from "@expo/vector-icons";
import { AppLoading } from "expo";

const logger = createLogger({
	// ...options
});

console.disableYellowBox = true;
const client = {
	zaz: {
		client: axios.create({
			baseURL: "http://192.168.1.71/zaz/public/api/",
			responseType: "json"
		})
	},
	github: {
		client: axios.create({
			baseURL: "https://api.github.com",
			responseType: "json"
		})
	}
};

const persistConfig = {
	key: "root",
	storage: AsyncStorage,
	blacklist: ["navigation", "reducer"]
};

const persistedReducer = persistReducer(persistConfig, reducer);

const store = createStore(
	persistedReducer,
	composeWithDevTools(applyMiddleware(multiClientMiddleware(client), logger))
);
let persistor = persistStore(store);
export default class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isReady: false
		};
	}

	async componentDidMount() {
		await Font.loadAsync({
			Roboto: require("native-base/Fonts/Roboto.ttf"),
			Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
			"SFUIDisplay-Semibold": require("./assets/fonts/SF-UI-Display-Semibold.ttf"),
			"SFUIDisplay-Regular": require("./assets/fonts/SF-UI-Display-Regular.ttf"),
			"SFUIDisplay-Light": require("./assets/fonts/SF-UI-Display-Regular.ttf"),

			...Ionicons.font
		});
		this.setState({ isReady: true });
	}
	render() {
		if (!this.state.isReady) {
			return <AppLoading />;
		}
		return (
			<Provider store={store}>
				<PersistGate loading={null} persistor={persistor}>
					<Router />
				</PersistGate>
			</Provider>
		);
	}
}
