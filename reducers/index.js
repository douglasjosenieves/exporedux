import { combineReducers } from "redux";
import reducer from "./reducer";
import loginphp from "./loginphp";

export default combineReducers({
	reducer: reducer,
	loginphp: loginphp
});
