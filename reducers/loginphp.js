const initialState = {
	name: "",
	token: "",
	repos: [],
	loading: false
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case "LOAD":
			return { ...state, loading: true };
		case "SUCCESS":
			return {
				...state,
				loading: false,
				repos: payload.data,
				name: payload.data.user.name,
				token: payload.data.token
			};
		case "FAILURE":
			return {
				...state,
				loading: false,
				error: "Error while fetching repositories"
			};

		case "LOGOUT":
			return { ...state, name: "", token: "", repos: [] };

		default:
			return state;
	}
};
