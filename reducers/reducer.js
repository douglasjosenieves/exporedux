const initialState = {
	APP_NAME: "WebLink",
	URL: "https://tuweblink.com/",
	NOMBRE: "Douglas Nieves"
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case "SET_APP_NAME":
			return { ...state, APP_NAME: payload.data };

		case "LOAD1":
			return { ...state, loading: true };

		case "SUCCESS1":
			return {
				...state,
				loading: false,
				repos: payload.data
			};

		case "FAILURE1":
			return {
				...state,
				loading: false,
				error: "Error while fetching repositories"
			};

		default:
			return state;
	}
};
