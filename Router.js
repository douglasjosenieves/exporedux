import React from "react";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import Detalle from "./components/Detalle";
import Article from "./components/Article";
//import Home from './components/Home';
import Home from "./Drawer/DrawerSocial/index";
import Login from "./SignIn/Signin_03/index.js";
//import Login from './components/Login';
import RepoList from "./components/RepoList";
import RepoList2 from "./components/RepoList2";

const AppNavigator = createStackNavigator(
  {
    Login: {
      screen: Login
    },
    Home: {
      screen: Home
    },
    Article: {
      screen: Article
    },

    Details: {
      screen: Detalle
    },

    RepoList: {
      screen: RepoList
    },

    RepoList2: {
      screen: RepoList2
    },

    Logout: {
      screen: Login
    }
  },
  {
    initialRouteName: "Login",
    mode: "modal",
    headerMode: "none",
    navigationOptions: {
      gesturesEnabled: false
    }
  }
);

export default createAppContainer(AppNavigator);
