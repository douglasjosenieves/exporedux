import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import * as action from "../actions/actions";

class RepoList extends Component {
  componentDidMount() {
    this.props.listRepos("relferreira");
  }
  renderItem = ({ item }) => (
    <TouchableOpacity onPress={() => this.actionOnRow(item)}>
      <View style={styles.item}>
        <Text>{item.name}</Text>
      </View>
    </TouchableOpacity>
  );

  actionOnRow(item) {
    console.log("Selected Item :", item);
  }

  render() {
    const { loading, repos } = this.props.reducer;

    if (loading == true) {
      return (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <Text>cargando....</Text>
        </View>
      );
    }

    return (
      <FlatList
        keyExtractor={(item, index) => index.toString()}
        styles={styles.container}
        data={repos}
        renderItem={this.renderItem}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  item: {
    padding: 16,
    borderBottomWidth: 1,
    borderBottomColor: "#ccc"
  }
});

const mapStateToProps = state => {
  let data = state.reducer;

  return {
    reducer: data
  };
};

export default connect(
  mapStateToProps,
  action
)(RepoList);
