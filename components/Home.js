"use strict";

import React from "react";

import { View } from "react-native";

import { createAppContainer } from "react-navigation";

import { createStackNavigator } from "react-navigation-stack";
import { createStore } from "redux";
import dummyReducer from "../reducers/reducer2";
import * as action from "../actions/actions";
import { connect } from "react-redux";
import Detalle from "./Detalle";
import {
	Container,
	Header,
	Content,
	Form,
	Item,
	Input,
	Label,
	Button,
	Text,
	Left,
	Body,
	Title,
	Right,
	Icon
} from "native-base";
class Home extends React.Component {
	static navigationOptions = {
		title: "Home"
	};
	constructor(props) {
		super(props);

		this.state = { email: "", clave: "" };
	}

	_logout = () => {
		this.props.logout();
		this.props.navigation.navigate("Login");
	};
	render() {
		console.log(this.props);

		return (
			<Container>
				<Header>
					<Left>
						<Button transparent>
							<Icon name="home" />
							<Text>Back</Text>
						</Button>
					</Left>
					<Body>
						<Title>Home ({this.props.loginphp.name})</Title>
					</Body>
					<Right />
				</Header>
				<Content>
					<View style={{ margin: 50 }}>
						<Button
							style={{ marginBottom: 20 }}
							title="Go to Details"
							onPress={() => {
								// store.dispatch(SET_TEXT('John Doe'));
								this.props.set_text("Douglas");

								this.props.navigation.navigate("Details");
							}}
						>
							<Text>detalle</Text>
						</Button>

						<Button
							style={{ marginBottom: 20 }}
							title="Go to Lista"
							onPress={() => {
								// store.dispatch(SET_TEXT('John Doe'));

								this.props.navigation.navigate("RepoList");
							}}
						>
							<Text>Lista</Text>
						</Button>

						<Button
							danger
							title="Go to Lista"
							onPress={this._logout}
						>
							<Text>Log Out</Text>
						</Button>
					</View>
				</Content>
			</Container>
		);
	}
}

const mapStateToProps = state => {
	let data = state.loginphp;
	return {
		loginphp: data
	};
};

export default connect(
	mapStateToProps,
	action
)(Home);
