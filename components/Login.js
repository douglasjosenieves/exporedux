"use strict";

import React from "react";

import { View } from "react-native";

import { createAppContainer } from "react-navigation";

import { createStackNavigator } from "react-navigation-stack";
import { createStore } from "redux";
import dummyReducer from "../reducers/reducer2";
import * as action from "../actions/actions";
import { connect } from "react-redux";
import ValidationComponent from "react-native-form-validator";

import Detalle from "./Detalle";
import {
	Container,
	Header,
	Content,
	Form,
	Item,
	Input,
	Label,
	Button,
	Text,
	Left,
	Body,
	Title,
	Right,
	Icon
} from "native-base";
class Login extends ValidationComponent {
	static navigationOptions = {
		title: "Login"
	};
	constructor(props) {
		super(props);

		this.state = { email: "", clave: "" };
	}

	componentDidMount() {
		this.props.loginphp.loading = false;
		if (this.props.loginphp.token != "") {
			this.props.navigation.navigate("Home");
		}
	}

	_login = async () => {
		this.validate({
			clave: { required: true },
			email: { email: true, required: true }
		});

		if (this.isFormValid()) {
			try {
				let response = await this.props.login(this.state);

				if (response.type === "SUCCESS") {
					this.props.navigation.navigate("Home");
				} else {
					alert("Correo electrónico o contraseña inválidos");
				}
			} catch (error) {}
		}
	};

	render() {
		const { loading } = this.props.loginphp;

		return (
			<Container>
				<Header>
					<Left>
						<Button transparent>
							<Icon name="ios-key" />
							<Text>Back</Text>
						</Button>
					</Left>
					<Body>
						<Title>Login</Title>
					</Body>
					<Right />
				</Header>
				<Content>
					<Form>
						<Item floatingLabel>
							<Label>Email</Label>
							<Input
								ref="email"
								onChangeText={email => this.setState({ email })}
								value={this.state.email}
							/>
						</Item>
						{this.isFieldInError("email") &&
							this.getErrorsInField("email").map(errorMessage => (
								<Text
									style={{
										fontSize: 12,
										color: "#FA6F6F",
										textAlign: "center"
									}}
								>
									{errorMessage}
								</Text>
							))}

						<Item floatingLabel last>
							<Label>Password</Label>
							<Input
								ref="clave"
								onChangeText={clave => this.setState({ clave })}
								value={this.state.clave}
								type="password"
								secureTextEntry={true}
							/>
						</Item>

						{this.isFieldInError("clave") &&
							this.getErrorsInField("clave").map(errorMessage => (
								<Text
									style={{
										fontSize: 12,
										color: "#FA6F6F",
										textAlign: "center"
									}}
								>
									{errorMessage}
								</Text>
							))}

						{loading === true ? (
							<Button warning block>
								<Text>Cargando...</Text>
							</Button>
						) : (
							<Button block onPress={this._login}>
								<Text>Iniciar</Text>
							</Button>
						)}
					</Form>
				</Content>
			</Container>
		);
	}
}

const mapStateToProps = state => {
	let data = state.loginphp;
	return {
		loginphp: data
	};
};

export default connect(
	mapStateToProps,
	action
)(Login);
