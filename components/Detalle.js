import React from "react";
import { View, Text, Button } from "react-native";
import { createAppContainer } from "react-navigation";
import * as action from "../actions/actions";
import { createStackNavigator } from "react-navigation-stack";
import { createStore } from "redux";

import { connect } from "react-redux";

class Detalle extends React.Component {
	render() {
		//console.log(this.props);
		const { text } = this.props.reducer;
		return (
			<View
				style={{
					flex: 1,
					alignItems: "center",
					justifyContent: "center"
				}}
			>
				<Text>Details Screen</Text>
				<Text>{text}</Text>

				<Button
					title="Go to Home"
					onPress={() => {
						this.props.set_text("Francys");
						this.props.navigation.navigate("Home");
					}}
				/>
			</View>
		);
	}
}

const mapStateToProps = state => {
	let data = state.reducer;
	return {
		reducer: data
	};
};

export default connect(
	mapStateToProps,
	action
)(Detalle);
