export const GET_REPOS = "my-awesome-app/repos/LOAD";
export const GET_REPOS_SUCCESS = "my-awesome-app/repos/LOAD_SUCCESS";
export const GET_REPOS_FAIL = "my-awesome-app/repos/LOAD_FAIL";
export const SET_TEXT = "SET_TEXT";
export const LOGINRN2 = "LOGINRN2";

export const loginrn = data => {
	return {
		type: LOGINRN,
		payload: {
			data
		}
	};
};

export const login = data => {
	return {
		types: ["LOAD", "SUCCESS", "FAILURE"],
		payload: {
			client: "zaz",
			request: {
				url: `login`,
				method: "post",
				data: {
					email: data.email,
					password: data.clave
				}
			}
		}
	};
};

export const logout = payload => ({
	type: "LOGOUT",
	payload
});

export const set_text = data => {
	return {
		type: SET_TEXT,
		payload: {
			data
		}
	};
};

export const SET_APP_NAME = data => {
	return {
		type: "SET_APP_NAME",
		payload: {
			data
		}
	};
};

export function listRepos(user) {
	//alert(1);
	return {
		types: ["LOAD1", "SUCCESS1", "FAILURE1"],
		payload: {
			client: "github",
			request: {
				url: `/users/${user}/repos`
			}
		}
	};
}

export const uploadImage = formData => {
	return {
		types: ["UPLOAD-IMAGE", "UPLOAD-IMAGE-SUCCESS", "FAILURE"],
		payload: {
			client: "zaz",
			request: {
				url: `upload-image`,
				method: "post",
				headers: {
					Accept: "application/json",
					"Content-Type": "multipart/form-data"
				},
				data: formData
			}
		}
	};
};
