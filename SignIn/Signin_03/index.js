import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  StatusBar,
  Platform,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  BackHandler,
  I18nManager
} from "react-native";
import {
  Container,
  Button,
  Icon,
  Right,
  Item,
  Input,
  Header,
  Footer,
  Left,
  Body,
  Title,
  Content,
  Form,
  Label
} from "native-base";

import * as action from "../../actions/actions";
import { connect } from "react-redux";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import ValidationComponent from "react-native-form-validator";
import { StackActions, NavigationActions } from "react-navigation";
// Screen Styles
import styles from "./styles";
import { AppLoading } from "expo";
class Signin_03 extends ValidationComponent {
  constructor(props) {
    super(props);

    this.state = { email: "", clave: "" };
  }

  componentDidMount() {
    console.log(this.props);
    if (this.props.loginphp.token != "") {
      const resetAction = StackActions.reset({
        index: 0,

        actions: [NavigationActions.navigate({ routeName: "Home" })]
      });

      this.props.navigation.dispatch(resetAction);
    }

    // var that = this;
    // BackHandler.addEventListener("hardwareBackPress", function() {
    //   that.props.navigation.navigate("Home");
    //   return true;
    // });
  }

  render() {
    StatusBar.setBarStyle("light-content", true);
    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("transparent", true);
      StatusBar.setTranslucent(true);
    }

    let BG_Image = {
      uri:
        "https://antiqueruby.aliansoftware.net/Images/signin/signin_header_bg_sthree.png"
    };
    let Profile_Image = {
      uri:
        "https://antiqueruby.aliansoftware.net/Images/signin/profile_image_sthree.png"
    };

    // console.log(this.props.loginphp.loading);

    return (
      <Container>
        <ImageBackground style={styles.ImageBackground} source={BG_Image}>
          <Header style={styles.header}>
            <Left style={styles.left} />
            <Body style={styles.body}>
              <Text style={styles.textTitle}>Sign In</Text>
            </Body>
            <Right style={styles.right} />
          </Header>
        </ImageBackground>
        <View style={styles.userProfileSec}>
          <Image style={styles.profile} source={Profile_Image} />
        </View>
        <Content style={styles.content}>
          <Form style={styles.form}>
            <Item>
              <Input
                ref="email"
                onChangeText={email => this.setState({ email })}
                keyboardType="email-address"
                placeholder="Email"
                textAlign={I18nManager.isRTL ? "right" : "left"}
                placeholderTextColor="#b7b7b7"
                style={styles.input1}
              />
            </Item>
            {this.isFieldInError("email") &&
              this.getErrorsInField("email").map(errorMessage => (
                <Text style={{ color: "#F85555", fontSize: 12 }}>
                  {errorMessage}
                </Text>
              ))}

            <Item>
              <Input
                ref="clave"
                onChangeText={clave => this.setState({ clave })}
                placeholder="Password"
                textAlign={I18nManager.isRTL ? "right" : "left"}
                secureTextEntry={true}
                placeholderTextColor="#b7b7b7"
                style={styles.input1}
              />

              <TouchableOpacity
                style={styles.forgotlabelBg}
                onPress={() => alert("Forgot Password")}
              >
                <Label style={styles.forgotlabel}>Forgot password?</Label>
              </TouchableOpacity>
            </Item>
            {this.isFieldInError("clave") &&
              this.getErrorsInField("clave").map(errorMessage => (
                <Text style={{ color: "#F85555", fontSize: 12 }}>
                  {errorMessage}
                </Text>
              ))}
          </Form>
        </Content>
        <Footer>
          <TouchableOpacity
            style={styles.btnSec}
            disabled={!this.props.loginphp.loading ? false : true}
            onPress={this._enviar}
          >
            <Text autoCapitalize="words" style={styles.buttontext}>
              {!this.props.loginphp.loading ? "Sign In" : "Autentificando..."}
            </Text>
          </TouchableOpacity>
        </Footer>
      </Container>
    );
  }

  _enviar = async () => {
    this.validate({
      email: { email: true, required: true },
      clave: { required: true }
    });

    if (this.isFormValid()) {
      try {
        let response = await this.props.login(this.state);
        console.log(response);
        if (response.type === "SUCCESS") {
          const resetAction = StackActions.reset({
            index: 0,

            actions: [NavigationActions.navigate({ routeName: "Home" })]
          });

          this.props.navigation.dispatch(resetAction);
        } else {
          //   alert(respose.error.data);
          alert(
            response.error.data ??
              "Verifique sus credenciales y vuelva a intentarlo"
          );
        }
      } catch (error) {}
    }
  };
}

const mapStateToProps = state => {
  let data = state.loginphp;
  return {
    loginphp: data
  };
};

export default connect(
  mapStateToProps,
  action
)(Signin_03);
